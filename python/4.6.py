#!/usr/bin/env python3

from cmath import *
from numerical_integration import *

for (body, l, u) in [
        ('(1 + x ** 3) ** -0.5', 0, 2),
        ('sqrt(x) * sin(x)', pi / 2, pi),
        ('tan(x ** 2)', 0, sqrt(pi / 4)),
        ('sqrt(1 + sin(x) ** 2)', 0, pi / 2),
        ('sin(x) / x if x else 1', 0, pi),
        ]:
    fn = lambda x: eval(body)
    t = trapezoidal(fn, l, u, 4)
    s = simpsons(fn, l, u, 4)
    print(f'[{l}, {u}] {body}:'
          f'\n  Trapezoidal: {t:3f}'
          f'\n  Simpsons   : {s:3f}')
