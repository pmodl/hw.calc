#!/usr/bin/env python3

from math import *

def mapinterval(fn: callable, lower: float, upper: float, n: int):
    return map(fn, [lower + (upper - lower) * i / n for i in range(n + 1)])

def trapezoidal(fn, lower, upper, n):
    [fa, *fxs, fb] = mapinterval(fn, lower, upper, n)
    return (upper - lower) / 2 / n * (fa + 2 * sum(fxs) + fb)

def simpsons(fn, lower, upper, n):
    [fa, *fxs, fb] = mapinterval(fn, lower, upper, n)
    return (upper - lower) / 3 / n * (fa + 4 * sum(fxs[0::2]) + 2 * sum(fxs[1::2]) + fb)


